from __future__ import print_function, division
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from features import FEATURES
from samples import DIRECTORY, CAMPAIGNS, SIGNAL, BACKGROUND

iopt = 0

def objective(num_folds, outFile, space):
    global iopt

    dataloader = ROOT.TMVA.DataLoader("hyperparameter_optmization_dataset")

    for feature in FEATURES:
        dataloader.AddVariable(*feature)

    signalChain = ROOT.TChain("nominal")
    backgroundChain = ROOT.TChain("nominal")
    for campaign in CAMPAIGNS:
        for signal_sample in SIGNAL:
            file = '/'.join([DIRECTORY,campaign,signal_sample])
            signalChain.Add(file)
               
        for background_sample in BACKGROUND:
            file = '/'.join([DIRECTORY,campaign,background_sample])
            backgroundChain.Add(file)

    dataloader.AddTree(signalChain, "Signal")
    dataloader.AddTree(backgroundChain, "Background")

    dataloader.SetSignalWeightExpression("(mcWeightOrg*mcWeight*pileupEventWeight*jvtEventWeight*leptonEventWeight*jPsiEventWeight*bjetEventWeight)/totalEventsWeighted")
    dataloader.SetBackgroundWeightExpression("(mcWeightOrg*mcWeight*pileupEventWeight*jvtEventWeight*leptonEventWeight*jPsiEventWeight*bjetEventWeight)/totalEventsWeighted")

    sigCut = ROOT.TCut("(mcWeightOrg*mcWeight*pileupEventWeight*jvtEventWeight*leptonEventWeight*jPsiEventWeight*bjetEventWeight)/totalEventsWeighted>0.0")
    bkgCut = ROOT.TCut("(mcWeightOrg*mcWeight*pileupEventWeight*jvtEventWeight*leptonEventWeight*jPsiEventWeight*bjetEventWeight)/totalEventsWeighted>0.0")


    dataloader.PrepareTrainingAndTestTree(sigCut,
                                          bkgCut,
                                          ":".join(["NormMode=None",
                                                    "!V"]
                                                    ))
    # factory = ROOT.TMVA.Factory("TMVAClassification", outFile,
    #                         ":".join([
    #                                     "!V",
    #                                     "Silent",
    #                                     "Transformations=I;D;P;G;D",
    #                                     "AnalysisType=Classification"]
    #                                 ))
    jobname = ROOT.TString("cv_hyperparameter_optimize")
    splitExpression = "int(fabs([eventNumber]))%int([NumFolds])" 
    opts = ":".join([
                    "!V",
                    "Silent",
                    "AnalysisType=Classification",
                    "NumFolds={}".format(num_folds),
                    "SplitType=Deterministic",
                    "Transformations=I;D;P;G;D"
                    "SplitExpr={}".format(splitExpression)
                    ])
    topts = ROOT.TString(opts)

    cv = TMVA.CrossValidation(jobname,dataloader,outFile,topts)

    method_options = ['!H','!V']
    boost_options = ['{0}={1!s}'.format(*param) for param in space.pop('BoostType').iteritems()]
    general_options = ['{0}={1!s}'.format(*param) for param in space.iteritems()]
    cv.BookMethod(ROOT.TMVA.Types.kBDT, "BDT"+str(iopt),':'.join(method_options + boost_options + general_options))

    cv.Evaluate()
    results = cv.GetResults()

    # factory.TrainAllMethods()
    # factory.TestAllMethods()
    # factory.EvaluateAllMethods()
    outFile.Write()
    ROCint = 1.0 - results[0].GetROCAverage()
    # ROCint = 1.0 - factory.GetMethod(dataloader.GetName(),"BDT"+str(iopt)).GetROCIntegral()
    factory = cv.GetFactory()
    factory.GetMethod(dataloader.GetName(),"BDT"+str(iopt)).Reset()

    del factory, cv, dataloader
    iopt += 1
    return {'loss':ROCint, 'status':STATUS_OK}


HYPERPARAM_SPACE = hp.choice('TMVA_BDT', [
    {
        'NTrees': hp.quniform('NTrees', 100, 2000, 1),
        'MaxDepth': hp.quniform('MaxDepth', 2, 10, 1),
        'MinNodeSize': hp.uniform('MinNodeSize', 0, 10),
        'nCuts': hp.quniform('nCuts', 2, 50, 1),
        'BoostType': hp.choice('BoostType', [
            {
                'BoostType': 'AdaBoost',
                'AdaBoostBeta': hp.loguniform('AdaBoost_Beta', -5, 1),
                'NodePurityLimit': hp.uniform('AdaBoost_NodePurityLimit', 0, 1),
            },
            {
                'BoostType': 'RealAdaBoost',
                'AdaBoostBeta': hp.loguniform('RealAdaBoost_Beta', -5, 1),
                'UseYesNoLeaf': hp.choice('UseYesNoLeaf', [
                    'True',
                    'False',
                ]),
                'SigToBkgFraction': hp.uniform('SigToBkgFraction', 0, 1),
            },
            {
                'BoostType': 'Bagging',
                'BaggedSampleFraction': hp.uniform('BaggedSampleFraction', 0, 1),
                'NodePurityLimit': hp.uniform('Bagging_NodePurityLimit', 0, 1),
            },
            {
                'BoostType': 'Grad',
                'Shrinkage': hp.loguniform('Shrinkage', -5, 1),
                'NodePurityLimit': hp.uniform('Grad_NodePurityLimit', 0, 1),
            },
        ]),
        'UseBaggedBoost': hp.choice('UseBaggedBoost', [
            'True',
            'False',
        ]),
        'SeparationType': hp.choice('SeparationType', [
            'CrossEntropy',
            'GiniIndex',
            'MisClassificationError',
            'SDivSqrtSPlusB',
        ]),
    }
])

HYPERPARAM_CHOICE_MAP = {
    'BoostType': ['AdaBoost', 'RealAdaBoost', 'Bagging', 'Grad'],
    'UseYesNoLeaf': ['True', 'False'],
    'UseBaggedBoost': ['True', 'False'],
    'SeparationType': ['CrossEntropy', 'GiniIndex', 'MisClassificationError', 'SDivSqrtSPlusB'],
}


def train(num_trials, num_folds):
    outFile = ROOT.TFile("bdt_optimization.root", "RECREATE")

    trials = Trials()
    optimize = functools.partial(objective, num_folds, outFile)
    start_time = time.time()
    log.info('Starting hyperparameter optimization search at {}'.format(time.strftime('%Y-%m-%d-%H-%M-%S')))
    # log.info('Performing hyperparameter optimization search...')
    best = fmin(optimize, HYPERPARAM_SPACE, algo=tpe.suggest, max_evals=num_trials, trials=trials)
    log.debug('Trials: {}'.format(trials.trials))
    log.debug('Trial Results: {}'.format(trials.results))
    log.debug('Trial Losses: {}'.format(trials.losses()))
    log.info('Best Trial Hyperparameters: {}'.format(best))
    log.info('Best Trial Loss: {0:0.4f}'.format(min(trials.losses())))
    for hyperparam, value in best.iteritems():
        if hyperparam in HYPERPARAM_CHOICE_MAP:
            best[hyperparam] = HYPERPARAM_CHOICE_MAP[hyperparam][value]
    del best['TMVA_BDT']
    end_time = time.time() - start_time
    log.info('Search finished. Time taken: {}'.format(str(int(floor(end_time/3600)))+'h,'+str(int(floor(end_time/60)))+'m,'+str(int(round(end_time) % 60))))
    log.info('Best Trial TMVA BDT Options: {}'.format(':'.join('{0}={1!s}'.format(*param) for param in best.iteritems())))

if __name__ == '__main__':
    import argparse
    import time
    import functools
    import logging
    from math import floor

    __author__ = "D. Panchal"
    __doc__ = '''Optimize hyperparameters for BDT'''

    parser = argparse.ArgumentParser(usage='Usage: %(prog) s [options]', description=__doc__)
    parser.add_argument('--nTrials', type=int, default=10,
                        help="Number of trials for optimization")
    parser.add_argument('--nFolds', type=int, default=5,
                        help="Number of folds for cross validation")
    parsed = parser.parse_args()

    import ROOT
    from ROOT import TMVA
    # Ignore ROOT command line options
    ROOT.PyConfig.IgnoreCommandLineOptions = True
    ROOT.TMVA.Tools.Instance();

    num_trials = parsed.nTrials
    num_folds  = parsed.nFolds 

    log = logging.getLogger("optimize")
    logging_level = logging.DEBUG
    logging.basicConfig(filename='optimize_numTrials{0}_numFolds{1}.log'.format(num_trials,num_folds),
                        format='[%(name)s] %(levelname)s - %(message)s',
                        level=logging_level)

    train(num_trials, num_folds)
