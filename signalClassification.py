import ROOT
import os
from ROOT import TMVA
from features import FEATURES, SPECTATORS, EVENT_WEIGHT
from samples import SIGNAL, BACKGROUND, CAMPAIGNS, DIRECTORY

ROOT.TMVA.Tools.Instance()

outFile = ROOT.TFile("tmva_output_signal_background.root", "RECREATE")

factory = ROOT.TMVA.Factory("TMVAClassification", outFile,
                            ":".join([
                                        "!V",
                                        "!Silent",
                                        "Color",
                                        "DrawProgressBar",
                                        "Transformations=I;D;P;G;D",
                                        "AnalysisType=Classification"]
                                    ))

dataloader = ROOT.TMVA.DataLoader("dataset")


for feature in FEATURES:
    dataloader.AddVariable(*feature)

# for spectator in SPECTATORS:
#     dataloader.AddSpectator(spectator)

signalChain = ROOT.TChain("nominal")
backgroundChain = ROOT.TChain("nominal")
for campaign in CAMPAIGNS:
    for signal_sample in SIGNAL:
        file = '/'.join([DIRECTORY,campaign,signal_sample])
        signalChain.Add(file)
           
    for background_sample in BACKGROUND:
        file = '/'.join([DIRECTORY,campaign,background_sample])
        backgroundChain.Add(file)

dataloader.AddTree(signalChain, "Signal")
dataloader.AddTree(backgroundChain, "Background")

event_weight = EVENT_WEIGHT[0]
dataloader.SetSignalWeightExpression(event_weight)
dataloader.SetBackgroundWeightExpression(event_weight)

sigCut = ROOT.TCut("{}>0.0".format(event_weight))
bkgCut = ROOT.TCut("{}>0.0".format(event_weight))

dataloader.PrepareTrainingAndTestTree(sigCut,
                                      bkgCut,
                                      ":".join(["NormMode=None",
                                                "!V"]
                                                ))
dataloader.Print("v")
method = factory.BookMethod(dataloader, 
                            TMVA.Types.kBDT, 
                            "BDT0",
                            ":".join(["!H",
                                      "!V",
                                      "NTrees=100",
                                      "MinNodeSize=5%",
                                      "MaxDepth=3",
                                      "BoostType=AdaBoost",
                                      "AdaBoostBeta=0.5",
                                      "SeparationType=CrossEntropy",
                                      "nCuts=20",
                                      "PruneMethod=NoPruning",
                                      "DoBoostMonitor=True"])
                            )

method = factory.BookMethod(dataloader, 
                            TMVA.Types.kBDT, 
                            "BDT1",
                            ":".join(["!H",
                                      "!V",
                                      "NTrees=889",
                                      "UseYesNoLeaf=True",
                                      "nCuts=46",
                                      "MinNodeSize=3.78%",
                                      "MaxDepth=5",
                                      "UseBaggedBoost=False",
                                      "BoostType=RealAdaBoost",
                                      "AdaBoostBeta=0.11318",
                                      "SeparationType=GiniIndex",
                                      "SigToBkgFraction=0.9785",
                                      "DoBoostMonitor=True"]))

method = factory.BookMethod(dataloader, 
                            TMVA.Types.kBDT, 
                            "BDT2",
                            ":".join(["!H",
                                      "!V",
                                      "NTrees=1687.0",
                                      "MaxDepth=2.0",
                                      "nCuts=40.0",
                                      "UseBaggedBoost=False",
                                      "BoostType=Grad",
                                      "Shrinkage=0.0256465168966",
                                      "NodePurityLimit=0.207022537009",
                                      "MinNodeSize=2.79030509192%",
                                      "SeparationType=SDivSqrtSPlusB",
                                      "DoBoostMonitor=True"]))

method = factory.BookMethod(dataloader, 
                            TMVA.Types.kBDT,
                            "BDT3",
                            ":".join(["!H",
                                      "!V",
                                      "NTrees=1997.0",
                                      "UseYesNoLeaf=False",
                                      "MaxDepth=6.0",
                                      "nCuts=13.0",
                                      "UseBaggedBoost=True",
                                      "MinNodeSize=3.59279689321",
                                      "AdaBoostBeta=0.0458962711643",
                                      "BoostType=RealAdaBoost",
                                      "SeparationType=CrossEntropy",
                                      "SigToBkgFraction=0.737285629403"
                                      "DoBoostMonitor=True"]))

method = factory.BookMethod(dataloader,
                            TMVA.Types.kBDT,
                            "BDT4",
                            ":".join(["!H",
                                      "!V",
                                      "NTrees=1978",
                                      "UseYesNoLeaf=False",
                                      "UseBaggedBoost=True",
                                      "MaxDepth=6.0",
                                      "BoostType=RealAdaBoost",
                                      "nCuts=6.0",
                                      "MinNodeSize=4.02443237156",
                                      "AdaBoostBeta=0.135963780736",
                                      "SeparationType=CrossEntropy",
                                      "SigToBkgFraction=0.245716037332",
                                      "DoBoostMonitor=True"]
                                      ))

# factory.OptimizeAllMethods("ROCIntegral","Minuit");
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()