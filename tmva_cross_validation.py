#!/usr/bin/env python
"""
TMVA cross validation
Author: D. Panchal
Date: April 24, 2020

"""
from __future__ import print_function, division
import os
from array import array
from features import FEATURES, EVENT_WEIGHT
from samples import DIRECTORY, CAMPAIGNS, SIGNAL, BACKGROUND

def cross_validation(num_folds):
    ################## Signal and Background Trees ##################
    signal_chain = ROOT.TChain("nominal")
    background_chain = ROOT.TChain("nominal")

    for campaign in CAMPAIGNS:
      for signal_sample in SIGNAL:
        file = '/'.join([DIRECTORY,campaign,signal_sample])
        signal_chain.Add(file)

        for background_sample in BACKGROUND:
            file = '/'.join([DIRECTORY,campaign,background_sample])
            background_chain.Add(file)  

    ################## Create dataloader ##################
    dataloader = ROOT.TMVA.DataLoader("cross_validation_dataset")

    for feature in FEATURES:
        dataloader.AddVariable(*feature)

    dataloader.AddSpectator("eventNumber","F")

    dataloader.AddSignalTree(signal_chain)
    dataloader.AddBackgroundTree(background_chain)

    event_weight = EVENT_WEIGHT[0]
    dataloader.SetSignalWeightExpression(event_weight)
    dataloader.SetBackgroundWeightExpression(event_weight)

    # Add signal and background cuts.
    # Require positive event weights for training and testing.
    sigCut = ROOT.TCut("{}>0.0".format(event_weight))
    bkgCut = ROOT.TCut("{}>0.0".format(event_weight))

    # Create training and test datasets
    dataloader.PrepareTrainingAndTestTree(sigCut,
                                          bkgCut,
                                          ":".join([
                                                    "SplitMode=Random",
                                                    "NormMode=None",
                                                    "!V"
                                                    ])
                                          )
    # Output file to store the results
    output_file = ROOT.TFile("bdt_cross_validation.root","RECREATE")

    jobname = ROOT.TString("cross_validation")

    # Split the training and testing events by the number of folds 
    splitExpression = "int(fabs([eventNumber]))%int([NumFolds])"

    opts = ":".join([
                    "!V",
                    "AnalysisType=Classification",
                    "NumFolds={}".format(num_folds),
                    "SplitType=Deterministic",
                    "Transformations=I;D;P;G;D",
                    "SplitExpr={}".format(splitExpression)
                    ])
    topts = ROOT.TString(opts)

    cv = TMVA.CrossValidation(jobname,dataloader,output_file,topts)

    # Method 1
    cv.BookMethod( 
                  TMVA.Types.kBDT, 
                  "BDT0",
                  ":".join(["!H",
                            "!V",
                            "NTrees=100",
                            "MinNodeSize=5%",
                            "MaxDepth=3",
                            "BoostType=AdaBoost",
                            "AdaBoostBeta=0.5",
                            "SeparationType=CrossEntropy",
                            "nCuts=-1",
                            "PruneMethod=NoPruning",
                            "DoBoostMonitor=True"])
                                )

    # cv.BookMethod( 
    #               TMVA.Types.kBDT, 
    #               "BDT1",
    #               ":".join(["!H",
    #                       "!V",
    #                       "NTrees=889",
    #                       "UseYesNoLeaf=True",
    #                       "nCuts=46",
    #                       "MinNodeSize=3.78%",
    #                       "MaxDepth=5",
    #                       "UseBaggedBoost=False",
    #                       "BoostType=RealAdaBoost",
    #                       "AdaBoostBeta=0.11318",
    #                       "SeparationType=GiniIndex",
    #                       "SigToBkgFraction=0.9785",
    #                       "DoBoostMonitor=True"])
    #                             )

    # cv.BookMethod( 
    #               TMVA.Types.kBDT, 
    #               "BDT2",
    #               ":".join(["!H",
    #                         "!V",
    #                         "NTrees=1687.0",
    #                         "Shrinkage=0.0256465168966",
    #                         "nCuts=40",
    #                         "NodePurityLimit=0.207022537009",
    #                         "MinNodeSize=2.79030509192",
    #                         "UseBaggedBoost=False",
    #                         "MaxDepth=2.0",
    #                         "BoostType=Grad",
    #                         "SeparationType=SDivSqrtSPlusB",
    #                         "DoBoostMonitor=True"])
    #                             )


    cv.Evaluate()

    results = cv.GetResults()

    print("======================= Cross validation results =======================")
    for i,result in enumerate(results):
      print("Summary for method ", cv.GetMethods()[i].GetValue("MethodName"))
      for iFold in range(cv.GetNumFolds()):
        print("\tFold {0:0.3f} : ROC int: {1:0.3f}".format(iFold+1,result.GetROCValues()[iFold])) 
      print("\tAverage ROC integral: {0:0.3f} +- {1:0.3f}".format(result.GetROCAverage(), result.GetROCStandardDeviation()))

if __name__ == '__main__':
    import argparse
    
    __doc__ = "Cross validation using TMVA"
    parser = argparse.ArgumentParser(usage='Usage %(prog) s [options]', description=__doc__)
    parser.add_argument('-nf', '--numFolds', type=int, default=10,
                        help="Number of k-folds for cross validation. (Default 10).")
    parsed = parser.parse_args()
    
    import ROOT
    from ROOT import TMVA
    # Ignore ROOT command line options
    ROOT.PyConfig.IgnoreCommandLineOptions = True
    ROOT.TMVA.Tools.Instance();
    # Ignore ROOT command line options
    ROOT.PyConfig.IgnoreCommandLineOptions = True

    cross_validation(parsed.numFolds)

