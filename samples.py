DIRECTORY = '/data/dpanchal/ntuples21_new/'
CAMPAIGNS = ['mc16a','mc16d','mc16e']

tchan = [
            '411097_slimmed/user.dpanchal.411097.slimmed.root',
            '411098_slimmed/user.dpanchal.411098.slimmed.root',
        ]

ttbar = [
            '411149_slimmed/user.dpanchal.411149.slimmed.root',
            '411150_slimmed/user.dpanchal.411150.slimmed.root',
        ]

wt  =   [      
            '411113_slimmed/user.dpanchal.411113.slimmed.root',
            '411114_slimmed/user.dpanchal.411114.slimmed.root',
        ]

schan = [
            '411034_slimmed/user.dpanchal.411034.slimmed.root',
            '411035_slimmed/user.dpanchal.411035.slimmed.root',
        ]

wjets = [
            '361100_slimmed/user.dpanchal.361100.slimmed.root',
            '361101_slimmed/user.dpanchal.361101.slimmed.root',
            '361102_slimmed/user.dpanchal.361102.slimmed.root',
            '361103_slimmed/user.dpanchal.361103.slimmed.root',
            '361104_slimmed/user.dpanchal.361104.slimmed.root',
            '361105_slimmed/user.dpanchal.361105.slimmed.root',
            '364156_slimmed/user.dpanchal.364156.slimmed.root',
            '364157_slimmed/user.dpanchal.364157.slimmed.root',
            '364158_slimmed/user.dpanchal.364158.slimmed.root',
            '364159_slimmed/user.dpanchal.364159.slimmed.root',
            '364160_slimmed/user.dpanchal.364160.slimmed.root',
            '364162_slimmed/user.dpanchal.364162.slimmed.root',
            '364163_slimmed/user.dpanchal.364163.slimmed.root',
            '364164_slimmed/user.dpanchal.364164.slimmed.root',
            '364165_slimmed/user.dpanchal.364165.slimmed.root',
            '364166_slimmed/user.dpanchal.364166.slimmed.root',
            '364167_slimmed/user.dpanchal.364167.slimmed.root',
            '364168_slimmed/user.dpanchal.364168.slimmed.root',
            '364169_slimmed/user.dpanchal.364169.slimmed.root',
            '364170_slimmed/user.dpanchal.364170.slimmed.root',
            '364171_slimmed/user.dpanchal.364171.slimmed.root',
            '364172_slimmed/user.dpanchal.364172.slimmed.root',
            '364173_slimmed/user.dpanchal.364173.slimmed.root',
            '364175_slimmed/user.dpanchal.364175.slimmed.root',
            '364176_slimmed/user.dpanchal.364176.slimmed.root',
            '364177_slimmed/user.dpanchal.364177.slimmed.root',
            '364179_slimmed/user.dpanchal.364179.slimmed.root',
            '364180_slimmed/user.dpanchal.364180.slimmed.root',
            '364181_slimmed/user.dpanchal.364181.slimmed.root',
            '364182_slimmed/user.dpanchal.364182.slimmed.root',
            '364183_slimmed/user.dpanchal.364183.slimmed.root',
            '364184_slimmed/user.dpanchal.364184.slimmed.root',
            '364185_slimmed/user.dpanchal.364185.slimmed.root',
            '364186_slimmed/user.dpanchal.364186.slimmed.root',
            '364187_slimmed/user.dpanchal.364187.slimmed.root',
            '364188_slimmed/user.dpanchal.364188.slimmed.root',
            '364189_slimmed/user.dpanchal.364189.slimmed.root',
            '364190_slimmed/user.dpanchal.364190.slimmed.root',
            '364191_slimmed/user.dpanchal.364191.slimmed.root',
            '364192_slimmed/user.dpanchal.364192.slimmed.root',
            '364193_slimmed/user.dpanchal.364193.slimmed.root',
            '364194_slimmed/user.dpanchal.364194.slimmed.root',
            '364195_slimmed/user.dpanchal.364195.slimmed.root',
            '364196_slimmed/user.dpanchal.364196.slimmed.root',
            '364197_slimmed/user.dpanchal.364197.slimmed.root',
        ]

zjets = [       
            '361106_slimmed/user.dpanchal.361106.slimmed.root',
            '361107_slimmed/user.dpanchal.361107.slimmed.root',
            '361108_slimmed/user.dpanchal.361108.slimmed.root',
            '364100_slimmed/user.dpanchal.364100.slimmed.root',
            '364101_slimmed/user.dpanchal.364101.slimmed.root',
            '364104_slimmed/user.dpanchal.364104.slimmed.root',
            '364106_slimmed/user.dpanchal.364106.slimmed.root',
            '364107_slimmed/user.dpanchal.364107.slimmed.root',
            '364109_slimmed/user.dpanchal.364109.slimmed.root',
            '364110_slimmed/user.dpanchal.364110.slimmed.root',
            '364111_slimmed/user.dpanchal.364111.slimmed.root',
            '364113_slimmed/user.dpanchal.364113.slimmed.root',
            '364115_slimmed/user.dpanchal.364115.slimmed.root',
            '364116_slimmed/user.dpanchal.364116.slimmed.root',
            '364118_slimmed/user.dpanchal.364118.slimmed.root',
            '364119_slimmed/user.dpanchal.364119.slimmed.root',
            '364121_slimmed/user.dpanchal.364121.slimmed.root',
            '364122_slimmed/user.dpanchal.364122.slimmed.root',
            '364123_slimmed/user.dpanchal.364123.slimmed.root',
            '364124_slimmed/user.dpanchal.364124.slimmed.root',
            '364125_slimmed/user.dpanchal.364125.slimmed.root',
            '364126_slimmed/user.dpanchal.364126.slimmed.root',
            '364127_slimmed/user.dpanchal.364127.slimmed.root',
            '364129_slimmed/user.dpanchal.364129.slimmed.root',
            '364131_slimmed/user.dpanchal.364131.slimmed.root',
            '364132_slimmed/user.dpanchal.364132.slimmed.root',
            '364133_slimmed/user.dpanchal.364133.slimmed.root',
            '364134_slimmed/user.dpanchal.364134.slimmed.root',
            '364135_slimmed/user.dpanchal.364135.slimmed.root',
            '364136_slimmed/user.dpanchal.364136.slimmed.root',
            '364137_slimmed/user.dpanchal.364137.slimmed.root',
            '364138_slimmed/user.dpanchal.364138.slimmed.root',
            '364139_slimmed/user.dpanchal.364139.slimmed.root',
            '364140_slimmed/user.dpanchal.364140.slimmed.root',
            '364141_slimmed/user.dpanchal.364141.slimmed.root',
        ]

DATA = [
        '/data/dpanchal/ntuples21_new/data/data15_slimmed/user.dpanchal.data15.slimmed.root'
        '/data/dpanchal/ntuples21_new/data/data16_slimmed/user.dpanchal.data16.slimmed.root'
        '/data/dpanchal/ntuples21_new/data/data17_slimmed/user.dpanchal.data17.slimmed.root'
        '/data/dpanchal/ntuples21_new/data/data18_slimmed/user.dpanchal.data18.slimmed.root'
        ]
        
SIGNAL = tchan
BACKGROUND = ttbar + wjets + zjets + wt + schan